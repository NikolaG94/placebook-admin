<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Placebook API</title>

    <link rel="stylesheet" href="{{ asset('assets/css/placebook-doc-style.css') }}" />
    <script src="{{ asset('assets/js/all.js') }}"></script>


      </head>

  <body class="">
    <a href="#" id="nav-button">
      <span>
        NAV
        <img src="{{ asset('assets/img/logo/navbar.png') }}" />
      </span>
    </a>
    <div class="tocify-wrapper">
        <img src="{{ asset('assets/img/logo/logo.png') }}" />
                      <div id="toc">
      </div>
            </div>
    <div class="page-wrapper">
      <div class="dark-box"></div>
      <div class="content">
          <!-- START_INFO -->
<h1>Info</h1>
<p>Welcome to the generated API reference.
<a href="http://localhost/docs/collection.json">Get Postman Collection</a></p>
<!-- END_INFO -->
<h1>Api</h1>
<p>API methods are shown below.</p>
<!-- START_e7cd11970b3fe6d6c515ce3a4770d6a5 -->
<h2>Random Ad from database</h2>
<p>If param first is set then it retrieves Ad set to show as first</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-javascript">var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/v1/ads",
    "method": "GET",
    "headers": {
        "Authorization": "Bearer $2y$10$Uwx/QyBdHVVzbzwHypxg/OiAA0v6O4bRJkpFhZjo9W0QSW0Hd9qGm",
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});</code></pre>
<blockquote>
<p>Example response:</p>
</blockquote>
<pre><code class="language-json">{
    "message": "Successfully retrieved random advertisement.",
    "code":200,
    "entity": {
        "name":"Block LLC",
        "link":"http://littel.net/",
        "created_at":null,
        "updated_at":null,
        "deleted_at":null,
        "image":{
            "url":"images/d70e7f4d22fa259e50363174a251efb7.jpg",
            "created_at":null,
            "updated_at":null
        }
    }
}</code></pre>
<h3>HTTP Request</h3>
<p><code>GET api/v1/ads</code></p>
<p><code>HEAD api/v1/ads</code></p>
<!-- END_e7cd11970b3fe6d6c515ce3a4770d6a5 -->
<!-- START_f4500d42ed7bd386bf5a645dddf47379 -->
<h2>All featured</h2>
<p>All featured for a group by group name</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-javascript">var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/v1/groups/{name}/features",
    "method": "GET",
    "headers": {
        "Authorization": "Bearer $2y$10$Uwx/QyBdHVVzbzwHypxg/OiAA0v6O4bRJkpFhZjo9W0QSW0Hd9qGm",
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});</code></pre>
<blockquote>
<p>Example response:</p>
</blockquote>
<pre><code class="language-json">{
    "message": "Successfully retrieved featured for Banks",
    "code": 200,
    "entity": [
        {
            "name": "Nolan Inc",
            "phone": "373-576-6073 x3847",
            "address": "78813 Homenick Circles Suite 423\nDominicport, FL 54117-0330",
            "work_hours": "09:00 - 17:00",
            "email": "bgoldner@kassulke.biz",
            "timeout": 5,
            "created_at": null,
            "updated_at": null,
            "deleted_at": null,
            "thumbnail": {
                "url": "/images/bb5c2ee27167342c05dd2c8ab001fc99.jpg",
                "created_at": null,
                "updated_at": null
            }
        },
        {
            "name": "Gutkowski, Kassulke and Mante",
            "phone": "795.927.8226",
            "address": "72409 Bartoletti Lights Apt. 501\nWest Lera, WY 15919-8428",
            "work_hours": "09:00 - 17:00",
            "email": "borer.danielle@gottlieb.net",
            "timeout": 5,
            "created_at": null,
            "updated_at": null,
            "deleted_at": null,
            "thumbnail": {
                "url": "/images/b064f0b1b04e843ccf576b1cfa0bdbad.jpg",
                "created_at": null,
                "updated_at": null
            }
        },
        {
            "name": "Yost-King",
            "phone": "+1-261-633-3467",
            "address": "84849 Stamm Ridges Apt. 303\nKarlfurt, AK 25654",
            "work_hours": "09:00 - 17:00",
            "email": "ojakubowski@kshlerin.net",
            "timeout": 5,
            "created_at": null,
            "updated_at": null,
            "deleted_at": null,
            "thumbnail": {
                "url": "/images/916290a764479f2f3a12c2e02e78df2f.jpg",
                "created_at": null,
                "updated_at": null
            }
        },
        {
            "name": "Nicolas-Streich",
            "phone": "(706) 718-2816 x5961",
            "address": "73410 Kassulke Trace\nBrainton, IL 82093-4172",
            "work_hours": "09:00 - 17:00",
            "email": "vluettgen@hotmail.com",
            "timeout": 5,
            "created_at": null,
            "updated_at": null,
            "deleted_at": null,
            "thumbnail": {
                "url": "/images/ff305811af4fa71bae0ab1c9a93f6cbb.jpg",
                "created_at": null,
                "updated_at": null
            }
        },
        {
            "name": "Koelpin and Sons",
            "phone": "(385) 990-7483",
            "address": "211 Mayer Groves Apt. 764\nNorth Jacyntheport, ND 18145",
            "work_hours": "09:00 - 17:00",
            "email": "shea.koepp@hotmail.com",
            "timeout": 5,
            "created_at": null,
            "updated_at": null,
            "deleted_at": null,
            "thumbnail": {
                "url": "/images/07e3fe216e87d45e67e25a8c8c987677.jpg",
                "created_at": null,
                "updated_at": null
            }
        }
    ]
}</code></pre>
<h3>HTTP Request</h3>
<p><code>GET api/v1/groups/{name}/features</code></p>
<p><code>HEAD api/v1/groups/{name}/features</code></p>
<!-- END_f4500d42ed7bd386bf5a645dddf47379 -->
<!-- START_8c8c5516a469ff69c176f32b6548f830 -->
<h2>Featured by name</h2>
<p>Retrieve single featured by it's name</p>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-javascript">var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/v1/features/{name}",
    "method": "GET",
    "headers": {
        "Authorization": "Bearer $2y$10$Uwx/QyBdHVVzbzwHypxg/OiAA0v6O4bRJkpFhZjo9W0QSW0Hd9qGm",
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});</code></pre>
<blockquote>
<p>Example response:</p>
</blockquote>
<pre><code class="language-json">{
    "message": "Successfully retrieved featured for Nolan Inc",
    "code": 200,
    "entity": {
        "name": "Nolan Inc",
        "phone": "373-576-6073 x3847",
        "address": "78813 Homenick Circles Suite 423\nDominicport, FL 54117-0330",
        "work_hours": "09:00 - 17:00",
        "email": "bgoldner@kassulke.biz",
        "timeout": 5,
        "created_at": null,
        "updated_at": null,
        "deleted_at": null,
        "images": [
            {
                "url": "/images/bb5c2ee27167342c05dd2c8ab001fc99.jpg",
                "created_at": null,
                "updated_at": null
            },
            {
                "url": "/images/a0d40a707f50dc60be07ca23987b6830.jpg",
                "created_at": null,
                "updated_at": null
            },
            {
                "url": "/images/349336ab359f4cbf417ffb4754ec24ef.jpg",
                "created_at": null,
                "updated_at": null
            },
            {
                "url": "/images/b9a9108f899ea500da70411e7f4690be.jpg",
                "created_at": null,
                "updated_at": null
            }
        ]
    }
}</code></pre>
<h3>HTTP Request</h3>
<p><code>GET api/v1/features/{name}</code></p>
<p><code>HEAD api/v1/features/{name}</code></p>
<!-- END_8c8c5516a469ff69c176f32b6548f830 -->
      </div>
      <div class="dark-box">
                </div>
    </div>
  </body>
</html>