@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row text-center">
            <div class="col-md-12">
                <div class="title m-b-md">
                    Placebook
                </div>
            </div>
        </div>
        <div class="row text-center">
            <div class="col-md-offset-3 col-md-6">
                <div class="links">
                    <p class="col-md-3 col-xs-6"><a href="{{ route('group.create') }}" class="active">Dodaj Grupu</a></p>
                    <p class="col-md-3 col-xs-6"><a href="{{ route('featured.index') }}">Sponzorisani</a></p>
                    <p class="col-md-3 col-xs-6"><a href="{{ route('ad.index') }}">Reklame</a></p>
                    <p class="col-md-3 col-xs-6"><a href="{{ route('settings.show') }}">Podešavanja</a></p>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            @foreach($groups as $group)
                <div class="col-md-4">
                    <a href="{{ route('group.show', ['group' => $group]) }}" class="text-center">
                        <img class="img-responsive center-block img-circle blue-background" src="{{ asset($group->icon_url) }}" alt="{{ $group->name }}">
                        <h2>{{ $group->name }}</h2>
                    </a>
                </div>
                @if($loop->iteration%3==0)
                <hr class="col-md-12">
                @endif
            @endforeach
        </div>
    </div>
@endsection
