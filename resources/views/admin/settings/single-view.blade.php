@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row text-center">
            <div class="col-md-12">
                <div class="title m-b-md">
                    Placebook
                </div>
            </div>
        </div>
        <div class="row text-center">
            <div class="col-md-offset-3 col-md-6">
                <div class="links">
                    <p class="col-md-3 col-xs-6"><a href="{{ route('group.index') }}">Grupe</a></p>
                    <p class="col-md-3 col-xs-6"><a href="{{ route('featured.index') }}" class="active">Sponzorisani</a></p>
                    <p class="col-md-3 col-xs-6"><a href="{{ route('ad.index') }}">Reklame</a></p>
                    <p class="col-md-3 col-xs-6"><a href="{{ route('settings.show') }}">Podešavanja</a></p>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            {{ Form::model($settings, ['route' => ['settings.update', 'settings' => $settings], 'class' => 'form-horizontal', 'method' => 'PUT']) }}
                <div class="col-sm-3 col-md-3 col-xs-4 text-left border-right-black">
                    <div class="form-group">
                        {{ Form::label('name', 'Prva reklama', ['class' => 'col-sm-12 control-label']) }}
                        <p>&nbsp;</p>
                    </div>
                    <div class="form-group">
                        {{ Form::label('ad_timeout', 'Vreme promene', ['class' => 'col-sm-12 control-label']) }}
                    </div>
                </div>
                <div class="col-sm-offset-1 col-sm-8 col-xs-8">
                    <div class="form-group">
                        {{ Form::select('firstAd', $ads, $settings->firstAd?$settings->firstAd->id:null, ['class' => 'form-control', 'placeholder' => 'Izaberite prvu reklamu']) }}
                        <p class="help-block">Izaberite reklamu koja će uvek prva da se prikazuje u aplikaciji.</p>
                    </div>
                    <div class="form-group">
                        {{ Form::number('ad_timeout', old('ad_timeout'), ['class' => 'form-control', 'placeholder' => '5']) }}
                        <p class="help-block">Vreme promene reklama u sekundama. (5, 10, 60, 3600)</p>
                    </div>
                    <div class="form-group">
                        {{ Form::submit('Snimi', ['class' => 'btn btn-primary']) }}
                    </div>
                </div>
            {{ Form::close() }}
        </div>
    </div>
@endsection
