@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row text-center">
            <div class="col-md-12">
                <div class="title m-b-md">
                    Placebook
                </div>
            </div>
        </div>
        <div class="row text-center">
            <div class="col-md-offset-3 col-md-6">
                <div class="links">
                    <p class="col-md-3 col-xs-6"><a href="{{ route('group.index') }}">Grupe</a></p>
                    <p class="col-md-3 col-xs-6"><a href="{{ route('featured.index') }}" class="active">Sponzorisani</a></p>
                    <p class="col-md-3 col-xs-6"><a href="{{ route('ad.create') }}">Dodaj Reklamu</a></p>
                    <p class="col-md-3 col-xs-6"><a href="{{ route('settings.show') }}">Podešavanja</a></p>
                </div>
            </div>
        </div>
        <hr>
        <div class="row text-center">
            <h2><a href="{{ route('ad.edit', ['ad' => $ad]) }}">{{ $ad->name }}</a></h2>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-4">
                <p><img class="img-responsive" src="{{ $ad->image->url }}" alt="{{ $ad->name }}" /></p>
            </div>
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-12">
                        <p><i class="fa fa-link" aria-hidden="true"></i>
                        @if($ad->link)
                            <a class="btn-link" href="{{ $ad->link }}" target="_blank">{{ $ad->link }}</a>
                        @else
                            Nije dodat link za ovu reklamu.
                        @endif
                        </p>
                    </div>
                    <div class="col-md-12">
                        <p><strong>Da li je link aktivan?</strong>  <i class="fa {{ $ad->active?'fa-check':'fa-times' }}" aria-hidden="true"></i></p>
                    </div>
                    <div class="col-md-12">
                        <p><i class="fa fa-globe" aria-hidden="true"></i>
                            <strong>@if($ad->country=='world') Ceo svet @else {{ $ad->country }} @endif</strong>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
