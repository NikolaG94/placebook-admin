@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row text-center">
            <div class="col-md-12">
                <div class="title m-b-md">
                    Placebook
                </div>
            </div>
        </div>
        <div class="row text-center">
            <div class="col-md-offset-3 col-md-6">
                <div class="links">
                    <p class="col-md-3 col-xs-6"><a href="{{ route('group.index') }}">Grupe</a></p>
                    <p class="col-md-3 col-xs-6"><a href="{{ route('featured.index') }}">Sponzorisani</a></p>
                    <p class="col-md-3 col-xs-6"><a href="{{ route('ad.index') }}" class="active">Reklame</a></p>
                    <p class="col-md-3 col-xs-6"><a href="{{ route('settings.show') }}">Podešavanja</a></p>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-offset-2 col-md-8">
                <h1>Trenutno menjate <strong>{{ $ad->name }}</strong> reklamu</h1>
            </div>
        </div>
        <hr>
        @if (count($errors) > 0)
            <div class="row">
                <div class="col-md-offset-2 col-md-8">
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li class="normal-font">{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col-md-10">
                {{ Form::model($ad, ['route' => ['ad.update', $ad], 'class' => 'form-horizontal', 'method' => 'PUT', 'files' => true]) }}
                <div class="form-group">
                    {{ Form::label('name', 'Ime reklame *', ['class' => 'col-sm-2 control-label']) }}
                    <div class="col-sm-10">
                        {{ Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => 'Ime reklame']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('link', 'Link reklame', ['class' => 'col-sm-2 control-label']) }}
                    <div class="col-sm-10">
                        {{ Form::text('link', old('link'), ['class' => 'form-control', 'placeholder' => 'http://www.google.rs/']) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('country', 'Država', ['class' => 'col-sm-2 control-label']) }}
                    <div class="col-sm-10">
                        {{ Form::text('country', old('country'), ['class' => 'form-control', 'placeholder' => 'Sweden']) }}
                        <div class="help-block"><strong>* Ukoliko je polje prazno ili piše "world", reklama se prikazuje svima.</strong></div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <div class="checkbox">
                            <label>
                                {{ Form::checkbox('active', null, old('active'), ['data-toggle' => 'checkbox']) }} Aktivna?
                            </label>
                            <div class="help-block"><strong>* Da li je reklama aktivna.</strong></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-offset-2 col-sm-3">
                        <p><img class="img-responsive" src="{{ $ad->image->url }}" alt="" /></p>
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('image', 'Slika *', ['class' => 'col-sm-2 control-label']) }}
                    <div class="col-sm-10">
                        {{ Form::file('image', []) }}
                        <p class="help-block"><strong>* Reklamna slika. (320x100 u pikselima)</strong></p>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        {{ Form::submit('Snimi', ['class' => 'btn btn-primary']) }}
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
@endsection
