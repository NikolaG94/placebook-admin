<footer class="col-md-12 footer">
    <div class="container">
        <div class="row">
            <div class="col-md-offset-2 col-md-2">
                <h5>Stranice</h5>
                <ul class="list-unstyled">
                    <li><a href="{{ route('group.index') }}" class="btn btn-link">Grupe</a></li>
                    <li><a href="{{ route('featured.index') }}" class="btn btn-link">Sponzorisani</a></li>
                    <li><a href="{{ route('ad.index') }}" class="btn btn-link">Reklame</a></li>
                    <li><a href="{{ route('settings.show', ['settings' => 1]) }}" class="btn btn-link">Podešavanja</a></li>
                </ul>
            </div>
            <div class="col-md-3">
                <h5>Korisni linkovi</h5>
                <a class="btn btn-link" href="{{ route('docs') }}">Docs</a>
            </div>
            <div class="col-md-2">
                <h5>E-Prodavnice</h5>
                <a target="_blank" class="pull-left" href='https://play.google.com/store/apps/details?id=com.placebook.mobile&hl=en'><img width="160" class="img-responsive" alt='Get it on Google Play' src='https://play.google.com/intl/en_us/badges/images/generic/en_badge_web_generic.png'/></a>
                <a target="_blank" class="pull-left padding-10" href='https://developer.apple.com/app-store/marketing/guidelines/#downloadOnAppstore'><img class="img-responsive" alt='Get it on Apple Store' src='{{ asset('assets/img/Download_on_the_App_Store_Badge_US-UK_135x40.svg') }}'/></a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 padding-top-20 text-left">
                <p>Powered with <a class="btn-link" href="http://clickbyclick.rs/" target="_blank">CBC&#8482;</a></p>
            </div>
            <div class="col-md-6 padding-top-20 text-right">
                <p>2018 &#169; <a class="btn-link" href="https://rs.linkedin.com/in/nikolagavric" target="_blank">Nikola Gavrić</a></p>
            </div>
        </div>
    </div>
</footer>
