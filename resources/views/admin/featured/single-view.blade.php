@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row text-center">
            <div class="col-md-12">
                <div class="title m-b-md">
                    Placebook
                </div>
            </div>
        </div>
        <div class="row text-center">
            <div class="col-md-offset-3 col-md-6">
                <div class="links">
                    <p class="col-md-3 col-xs-6"><a href="{{ route('group.index') }}" class="active">Grupe</a></p>
                    <p class="col-md-3 col-xs-6"><a href="{{ route('featured.index') }}" class="active">Sponzorisani</a></p>
                    <p class="col-md-3 col-xs-6"><a href="{{ route('ad.index') }}">Reklame</a></p>
                    <p class="col-md-3 col-xs-6"><a href="{{ route('settings.show') }}">Podešavanja</a></p>
                </div>
            </div>
        </div>
        <hr>
        <div class="row text-center">
            <h2><a href="{{ route('featured.edit', ['featured' => $featured]) }}">{{ $featured->name }}</a></h2>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-12">
                <img class="img-responsive" src="{{ $featured->thumbnail->url }}" alt="">
                <hr>
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <div class="row">
                            <div class="col-md-6 col-xs-6">
                                <img width="40" class="img-circle blue-background" src="{{ asset($featured->group->icon_url) }}" alt=""> {{ $featured->group->name }}
                            </div>
                            <div class="col-md-6 col-xs-6">
                                <i class="fa {{ $featured->active?'fa-check':'fa-times' }}" aria-hidden="true"></i> {{ $featured->active?' Sponzorisan':'Nije sponzorisan' }}
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <p class="col-md-12 col-xs-12">
                        <i class="fa fa-home" aria-hidden="true"></i> {{ $featured->name }}
                    </p>
                </div>
                <div class="row">
                    <p class="col-md-12 col-xs-12">
                        <i class="fa fa-clock-o" aria-hidden="true"></i> {{ $featured->work_hours }}
                    </p>
                </div>
                <div class="row">
                    <p class="col-md-12 col-xs-12">
                        <a href="http://maps.google.com/?q={{ title_case($featured->address) }}" target="_blank"><i class="fa fa-globe" aria-hidden="true"></i> {{ $featured->address }}</a>
                    </p>
                </div>
                <div class="row">
                    <p class="col-md-12 col-xs-12">
                        <a href="mailto:{{ $featured->email }}"><i class="fa fa-envelope-o" aria-hidden="true"></i> {{ $featured->email }}</a>
                    </p>
                </div>
                <div class="row">
                    <p class="col-md-12 col-xs-12">
                        <a href="tel:{{ $featured->phone }}"><i class="fa fa-phone" aria-hidden="true"></i> {{ $featured->phone }}</a>
                    </p>
                </div>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-12">
                <div id="featured-slides" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        @foreach($featured->images as $image)
                        <li data-target="#featured-slides" data-slide-to="{{ $loop->index }}" class="{{ ($loop->first)?'active':'' }}"></li>
                        @endforeach
                    </ol>


                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        @foreach($featured->images as $image)
                        @if($loop->first)
                        <div class="item active">
                        @else
                        <div class="item">
                        @endif
                            <img class="img-responsive" src="{{ $image->url }}" alt="">
                            <div class="carousel-caption">
                                Slide {{ $loop->iteration }}
                            </div>
                        </div>
                        @endforeach
                    </div>

                    <!-- Controls -->
                    <a class="left carousel-control" href="#featured-slides" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#featured-slides" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <hr>
</div>
@endsection
