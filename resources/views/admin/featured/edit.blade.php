@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row text-center">
        <div class="col-md-12">
            <div class="title m-b-md">
                Placebook
            </div>
        </div>
    </div>
    <div class="row text-center">
        <div class="col-md-offset-3 col-md-6">
            <div class="links">
                <p class="col-md-3 col-xs-6"><a href="{{ route('group.index') }}">Grupe</a></p>
                <p class="col-md-3 col-xs-6"><a href="{{ route('featured.index') }}" class="active">Sponzorisani</a></p>
                <p class="col-md-3 col-xs-6"><a href="{{ route('ad.index') }}">Reklame</a></p>
                <p class="col-md-3 col-xs-6"><a href="{{ route('settings.show') }}">Podešavanja</a></p>
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-offset-2 col-md-10">
            <h1>Trenutno menjate <strong>{{ $featured->name }}</strong> iz grupe <strong>{{ $featured->group->name }}</strong></h1>
        </div>
    </div>
    <hr>
    @if (count($errors) > 0)
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
    @endif
    <div class="row">
        <div class="col-md-12">
            {{ Form::model($featured, ['route' => ['featured.update', $featured], 'class' => 'form-horizontal', 'method' => 'PUT', 'files' => true]) }}
            <div class="form-group">
                {{ Form::label('name', 'Ime objekta', ['class' => 'col-sm-2 control-label']) }}
                <div class="col-sm-10">
                    {{ Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => 'Ime objekta']) }}
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('phone', 'Telefon objekta', ['class' => 'col-sm-2 control-label']) }}
                <div class="col-sm-10">
                    {{ Form::text('phone', old('phone'), ['class' => 'form-control', 'placeholder' => '+3816xxxxxxxx']) }}
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('address', 'Puna adresa objekta', ['class' => 'col-sm-2 control-label']) }}
                <div class="col-sm-10">
                    {{ Form::text('address', old('adress'), ['class' => 'form-control', 'placeholder' => 'Petra Petrovica 5']) }}
                    <p class="help-block"><strong>* Proveri prvo <a class="btn-link" href="https://www.google.com/maps" target="_blank">ovde</a> adresu pre snimanja</strong></p>
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('work_hours', 'Radno vreme objekta:', ['class' => 'col-sm-2 control-label']) }}
                <div class="col-sm-10">
                    {{ Form::text('work_hours', old('work_hours'), ['class' => 'form-control', 'placeholder' => '09:00 - 17:00']) }}
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('email', 'Mail adresa', ['class' => 'col-sm-2 control-label']) }}
                <div class="col-sm-10">
                    {{ Form::email('email', old('email'), ['class' => 'form-control', 'placeholder' => 'objekat@mail.com']) }}
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <div class="checkbox">
                        <label>
                            {{ Form::checkbox('active', null, old('active'), ['data-toggle' => 'checkbox']) }} Sponzorisan?
                        </label>
                        <div class="help-block"><strong>* Da li je objekat sponzorisan.</strong></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-offset-2 col-sm-3">
                    <p><img class="img-responsive" src="{{ $featured->thumbnail->url }}" alt=""></p>
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('thumbnail', 'Thumbnail', ['class' => 'col-sm-2 control-label']) }}
                <div class="col-sm-10">
                    {{ Form::file('thumbnail', []) }}
                    <p class="help-block"><strong>* Logo slika objekta.</strong></p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-offset-2">
                    @foreach($featured->images as $image)
                    <div class="col-sm-3">
                        <p><img class="img-responsive" src="{{ $image->url }}" alt=""></p>
                    </div>
                    @endforeach
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('images', 'Galerija', ['class' => 'col-sm-2 control-label']) }}
                <div class="col-sm-10">
                    {{ Form::file('images[]', ['multiple' => 'multiple']) }}
                    <p class="help-block"><strong>* Najviše 10 slika je dozvoljeno za upload. ({{ $featured->images->count() }} već postoji.)</strong></p>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    {{ Form::submit('Snimi', ['class' => 'btn btn-primary']) }}
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@endsection
