@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row text-center">
            <div class="col-md-12">
                <div class="title m-b-md">
                    Placebook
                </div>
            </div>
        </div>
        <div class="row text-center">
            <div class="col-md-offset-3 col-md-6">
                <div class="links">
                    <p class="col-md-3 col-xs-6"><a href="{{ route('group.index') }}">Grupe</a></p>
                    <p class="col-md-3 col-xs-6"><a href="{{ route('featured.index') }}" class="active">Sponzorisani</a></p>
                    <p class="col-md-3 col-xs-6"><a href="{{ route('ad.index') }}">Reklame</a></p>
                    <p class="col-md-3 col-xs-6"><a href="{{ route('settings.show') }}">Podešavanja</a></p>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-2 col-md-push-10 clearfix">
                {{ Form::open(['route' => 'featured.index', 'method' => 'GET', 'class' => 'form-horizontal']) }}
                <div class="form-group">
                    {{ Form::select('active', [null => 'Svi', 1 => 'Aktivni', 0 => 'Neaktivni'], request()->get('active'), ['class' => 'form-control']) }}
                </div>
                <div class="form-group">
                    {{ Form::text('search', request()->get('search'), ['class' => 'form-control', 'placeholder' => 'Traženi pojam']) }}
                </div>
                <div class="form-group pull-right">
                    {{ Form::submit('Pretraži', ['class' => 'btn btn-success']) }}
                </div>
                <div class="form-group pull-left">
                    <a class="btn btn-default" href="{{ route('featured.index') }}">Očisti</a>
                </div>
            </div>
            <div class="col-md-10 col-md-pull-2">
                @if($groups->count()>0)
                    @foreach($groups as $group)
                        @if($group->featured->count()>0)
                        <h2 class="text-center">{{ $group->name }}</h2>
                        <hr>
                        @endif
                        @foreach($group->featured as $feature)
                            @if($loop->iteration%4==1)
                            <div class="row">
                            @endif
                            <div class="col-sm-4 col-md-3 text-center">
                                <a href="{{ route('featured.show', ['featured' => $feature]) }}">
                                    <img class="img-responsive center-block" src="{{ asset($feature->thumbnail->url) }}" alt="{{ $feature->name }}">
                                    <h2>{{ $feature->name }}</h2>
                                </a>
                            </div>
                            @if($loop->iteration%4==0 || $loop->last)
                            </div>
                            @endif
                        @endforeach
                    @endforeach
                @else
                    <div class="col-sm-12">
                        <p class="lead">Nema pronađenih objekata ni za jednu grupu.</p>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
