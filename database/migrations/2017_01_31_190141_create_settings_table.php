<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Hash;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ad_timeout')->default(5);
            $table->integer('ad_id')->unsigned()->nullable();
            $table->string('api_token', 60)->default(Hash::make(env('APP_KEY')));
            $table->timestamps();
        });

        Schema::table('settings', function (Blueprint $table) {
            $table->foreign('ad_id')->references('id')->on('ads')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
