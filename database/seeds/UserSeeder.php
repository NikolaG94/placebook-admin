<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

class UserSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('users')->insert([
            [
                'name' => 'Nikola Gavrić',
                'email' => 'nikola.gavric94@gmail.com',
                'password' => Hash::make('123456'),
                'remember_token' => str_random(10),
                'created_at' => Carbon::now()
            ], [
                'name' => 'Miloš Zulfić',
                'email' => 'placebook2017@gmail.com',
                'password' => Hash::make('YxC2ln15i8'),
                'remember_token' => str_random(10),
                'created_at' => Carbon::now()
            ]
        ]);
    }
}
