<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $this->call(GroupSeeder::class);
        $this->call(UserSeeder::class);
        //$this->call(FeaturedSeeder::class);
        //$this->call(AdSeeder::class);
        $this->call(SettingsSeeder::class);
        //$this->call(ImageSeeder::class);
    }
}
