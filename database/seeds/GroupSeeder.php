<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GroupSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('groups')->insert([
            [
                'name' => 'Bank',
                'icon_url' => 'assets/img/groups/bank.png'
            ],
            [
                'name' => 'Caffe Bar',
                'icon_url' => 'assets/img/groups/caffe_bar.png'
            ],
            [
                'name' => 'Car Service',
                'icon_url' => 'assets/img/groups/car_service.png'
            ],
            [
                'name' => 'Cinema',
                'icon_url' => 'assets/img/groups/cinema.png'
            ],
            [
                'name' => 'Exchange Office',
                'icon_url' => 'assets/img/groups/exchange_office.png'
            ],
            [
                'name' => 'Gas Station',
                'icon_url' => 'assets/img/groups/gas_station.png'
            ],
            [
                'name' => 'Hospital',
                'icon_url' => 'assets/img/groups/hospital.png'
            ],
            [
                'name' => 'Hotel',
                'icon_url' => 'assets/img/groups/hotel.png'
            ],
            [
                'name' => 'Night Club',
                'icon_url' => 'assets/img/groups/night_club.png'
            ],
            [
                'name' => 'Parking',
                'icon_url' => 'assets/img/groups/parking.png'
            ],
            [
                'name' => 'Pharmacy',
                'icon_url' => 'assets/img/groups/pharmacy.png'
            ],
            [
                'name' => 'Restaurant',
                'icon_url' => 'assets/img/groups/restaurant.png'
            ],
            [
                'name' => 'Rent a Car',
                'icon_url' => 'assets/img/groups/rent_a_car.png'
            ],
            [
                'name' => 'Supermarket',
                'icon_url' => 'assets/img/groups/supermarket.png'
            ],
            [
                'name' => 'Shopping',
                'icon_url' => 'assets/img/groups/shopping.png'
            ],
            [
                'name' => 'Theater',
                'icon_url' => 'assets/img/groups/theater.png'
            ],
            [
                'name' => 'Taxi',
                'icon_url' => 'assets/img/groups/taxi.png'
            ],
        ]);
    }
}
