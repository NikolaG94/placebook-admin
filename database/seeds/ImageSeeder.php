<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class ImageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        //Ads
        for($i = 1; $i < 51; $i++) {
            DB::table('images')->insert([
                'url' => str_replace('public', '', $faker->image('public/images', 320, 100)),
                'is_thumbnail' => true,
                'imageable_id' => $i,
                'imageable_type' => 'App\Ad'
            ]);
        }

        //Featured
        for($i = 1; $i < 126; $i++) {
            DB::table('images')->insert([
                'url' => str_replace('public', '', $faker->image('public/images', 860, 640)),
                'is_thumbnail' => true,
                'imageable_id' => $i,
                'imageable_type' => 'App\Featured'
            ]);
        }

        //Featured gallery
        for($i = 1; $i < 350; $i++) {
            DB::table('images')->insert([
                'url' => str_replace('public', '', $faker->image('public/images', 860, 640)),
                'is_thumbnail' => false,
                'imageable_id' => $faker->numberBetween(1, 125),
                'imageable_type' => 'App\Featured'
            ]);
        }
    }
}
