<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class AdSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        for($i = 0; $i < 50; $i++) {
            DB::table('ads')->insert([
                'name' => $faker->unique()->company,
                'link' => $faker->unique()->url,
                'active' => true
            ]);
        }
    }
}
