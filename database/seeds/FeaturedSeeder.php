<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class FeaturedSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        $faker = Faker::create();
        $groupId = 1;

        for($i = 0; $i < 125; $i++) {
            $groupId = ($i%5==0 && $i!=0)?$groupId+1:$groupId;
            DB::table('featured')->insert([
                'name' => $faker->unique()->company,
                'phone' => $faker->phoneNumber,
                'address' => $faker->address,
                'work_hours' => '09:00 - 17:00',
                'email' => $faker->unique()->email,
                'group_id' =>  ($i<85)?$groupId:$faker->numberBetween(1, 17),
                'active' => ($i<110)?true:false
            ]);
        }
    }
}
