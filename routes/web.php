<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('welcome');

Auth::routes();

Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {
    /**
     * Group
     */
    Route::resource('group', 'GroupController', ['except' => [
        'destroy'
    ]]);

    /**
     * Featured
     */
    Route::get('featured/{group}/create', 'FeaturedController@create')->name('featured.create');
    Route::post('featured/{group}/store', 'FeaturedController@store')->name('featured.store');
    Route::resource('featured', 'FeaturedController', ['except' => [
        'create', 'store', 'destroy'
    ]]);

    /**
     * Ads
     */
    Route::resource('ad', 'AdController', ['except' => [
        'destroy'
    ]]);

    /**
     * Settings
     */
    Route::get('settings', 'SettingsController@show')->name('settings.show');
    Route::put('settings', 'SettingsController@update')->name('settings.update');

    /**
     * Docs
     */
    Route::get('api/docs', function () {
        return view('admin.docs.index');
    })->name('docs');
});
