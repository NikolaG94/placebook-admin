<?php
/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 1/29/17
 * Time: 16:51
 */

namespace App\Facades;


use Illuminate\Support\Facades\Facade;

class FileHandler extends Facade {

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() {
        return 'fileHandler';
    }
}