<?php
/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 1/20/17
 * Time: 04:16
 */

namespace App\Classes;


use Carbon\Carbon;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\File;

class FileHandler {
    public $currentPath;
    public $file;
    public $uploadPath;
    public $errors;
    public $savedPaths;

    /**
     * Default constructor
     * @param null $uploadPath
     */
    public function __construct($uploadPath = null) {
        $this->errors = collect([]);
        $this->savedPaths = collect([]);
        $this->currentPath = null;
        $this->uploadPath = $uploadPath;
    }

    /**
     * Path to first uploaded file
     *
     * @return mixed
     */
    public function first() {
        return $this->savedPaths->first();
    }

    /**
     * Path to the current file
     *
     * @return mixed
     */
    public function last() {
        return $this->savedPaths->last();
    }

    /**
     * Handles files upload
     * @param UploadedFile|null $file
     * @param \Illuminate\Http\UploadedFile $file
     * @return FileHandler
     */
    public function addFile(UploadedFile $file = null) {
        try {
            if ($this->uploadPath) {
                $this->file = $file;
                if ($this->file) {
                    $savedPath = md5(Carbon::now() . $file->getClientOriginalName()) . '.' . $file->getClientOriginalExtension();
                    $this->savedPaths->push($savedPath);
                    $file->move($this->uploadPath, $savedPath);
                }
            } else {
                throw new \Exception('\App\Classes\FileHandle::57 $uploadPath is not available');
            }
        } catch (\Exception $e) {
            $this->errors->push($e->getTraceAsString());
        }
        return $this->isSuccess();
    }

    /**
     * Handles removal of file
     * @param $source
     * @return $this
     */
    public function removeFile($source = null) {
        try {
            $this->currentPath = $source;
            if ($this->currentPath && File::exists($this->currentPath)) {
                File::delete($this->currentPath);
            }
        } catch (\Exception $e) {
            $this->errors->push($e->getMessage() . '; Line: ' . $e->getLine());
        }
        return $this->isSuccess();
    }

    /**
     * Checks whether there is an error
     * @return \App\Classes\FileHandler $this|\Illuminate\Http\JsonResponse
     */
    private function isSuccess() {
        try {
            if (!$this->errors->isEmpty()) {
                throw new \Exception($this->errors->first());
            }
            return $this;
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getTraceAsString(),
                'entity' => null,
                'code' => 500
            ], 500);
        }
    }

    /**
     * Sets a new upload path
     * @param $uploadPath
     * @return $this
     */
    public function uploadPath($uploadPath) {
        $this->uploadPath = $uploadPath;
        if ($this->uploadPath) {
            File::exists($this->uploadPath) or File::makeDirectory($this->uploadPath, 0777, true);
        }
        return $this;
    }

    /**
     * Creates a directory
     * @param $path
     */
    public function makeDir($path) {
        File::exists($path) or File::makeDirectory($path, 0777, true);
    }

    /**
     * Copies a file from source
     * to destination
     * @param $source
     * @param $destination
     */
    public function copy($source, $destination) {
        copy($source, $destination);
        return File::exists($destination);
    }

    /**
     * Find file by source path
     * @param $source
     * @return null
     */
    public function find($source) {
        try {
            return File::get($source);
        } catch (\Exception $e) {
            return null;
        }
    }

    /**
     * Find file extension by source path
     * @param $source
     * @return null
     */
    public function extension($source) {
        try {
            return File::extension($source);
        } catch (\Exception $e) {
            return null;
        }
    }

    /**
     * Delete recursively without deleting parent
     * @param $source
     */
    public function removeDir($source) {
        return File::deleteDirectory($source, true);
    }
}