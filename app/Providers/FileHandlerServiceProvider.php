<?php

namespace App\Providers;

use App\Classes\FileHandler;
use Illuminate\Support\ServiceProvider;

class FileHandlerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('fileHandler', function ($app) {
            return new FileHandler(null);
        });
    }
}
