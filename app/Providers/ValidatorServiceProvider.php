<?php

namespace App\Providers;

use App\Featured;
use App\Group;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class ValidatorServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('featured_max', function($attribute, $value, $parameters) {
            $count = Featured::where('active', true)->where('group_id', '=', $parameters[1])->count();
            return $count <= $parameters[0];
        });

        Validator::replacer('featured_max', function ($message, $attribute, $rule, $parameters) {
            $groupName = Group::find($parameters[1])->name;
            $parameters[1] = $groupName;
            return str_replace([':value', ':group'], $parameters, $message);
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
