<?php
/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 1/20/17
 * Time: 00:05
 */

namespace App\Helpers;


class Constant {
    //Class identifiers
    const AD_IDENTIFIER = 'App\Ad';
    const FEATURED_IDENTIFIER = 'App\Featured';
    const IMAGE_IDENTIFIER = 'App\Image';
    const GROUP_IDENTIFIER = 'App\Group';
    const USER_IDENTIFIER = 'App\User';
    //Assets routes
    const IMAGES_PATH = '/images/';
    const ASSETS_PATH = '/assets/';


    /**
     * @param $constant
     * @return string
     */
    public static function absolutePath($constant) {
        return public_path() . constant('self::' . $constant);
    }
}