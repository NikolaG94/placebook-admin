<?php

namespace App\Http\Controllers;

use App\Group;
use App\Helpers\Constant;
use App\Http\Requests\CreateGroupRequest;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class GroupController extends Controller {

    /**
     * GroupController constructor.
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $groups = Group::all();
        return view('admin.group.index', ['groups' => $groups]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $group = new Group;
        return view('admin.group.create', ['group' => $group]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateGroupRequest $request) {
        $group = new Group;
        $group->fill($request->all());
        $image = $request->image;
        $filePath = Constant::ASSETS_PATH . '/img/groups/' . $image->getClientOriginalName() . '.' . $image->getClientOriginalExtension();
        Image::make($image)->resize(71, 71)->save(public_path($filePath));
        $group->icon_url = $filePath;
        $group->save();
        return redirect()->route('group.index');
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param Group $group
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Request $request, Group $group) {

        $active = $request->has('active');
        $search = $request->has('search');

        $group->load(['featured' => function($query) use ($request, $search, $active) {
            $searchTerm = $request->get('search');
            $activeTerm = $request->get('active');
            $isNotEmpty = ($activeTerm==0 || $activeTerm==1) && $activeTerm!='';

            if($search && !empty($searchTerm))
                $query->where('name', 'LIKE', '%'.$searchTerm.'%')->get();
            if($active && $isNotEmpty)
                $query->where('active', $activeTerm == 1);
        }, 'featured.thumbnail', 'featured.images']);

        return view('admin.group.single-view', ['group' => $group]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }
}
