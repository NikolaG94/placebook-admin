<?php

namespace App\Http\Controllers;

use App\Featured;
use App\Group;
use Illuminate\Http\Request;
use App\Ad;
use App\Settings;
use App\Classes\GeoLocation as GeoLocation;
use Illuminate\Support\Facades\Log;


/**
 * @resource Api
 *
 * API methods are shown below.
 */
class ApiController extends Controller
{

    /**
     * Random Ad
     *
     * If param first is set then it retrieves Ad set to show as first
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function showAd(Request $request) {
        $objects = $this->getCountry($request->latitude, $request->longitude);
        $countryObj = $this->getGeocodeFromGeonames($request->latitude, $request->longitude);
        $isFirst = $request->has('first') && !empty($request->get('first'));
        $settings = Settings::with('firstAd')->find(1);

        if($isFirst) {
            $ad = $settings->firstAd;
            $ad->load('image');
            if($ad!=null) {
                $ad->timeout = $settings->ad_timeout;
                return response()->json([
                    'message' => 'Successfully retrieved first advertisement.',
                    'code' => 200,
                    'entity' => $ad
                ]);
            }
        }

        $ads = Ad::where('active', true)->where('name', '!=', $request->get('name'))->where(function($query) use ($countryObj) {
            $query->where('country', $countryObj->countryName)->orWhere('country', 'world')->orWhere('country', '');
        })->with('image')->get();

        if(!$ads->isEmpty()) {
            $ad = $ads->random();
            $ad->timeout = $settings->ad_timeout;
        } else {
            $ad = null;
        }
        return response()->json([
            'message' => 'Successfully retrieved random advertisement.',
            'code' => 200,
            'entity' => $ad
        ]);
    }

    /**
     * All featured
     *
     * All featured for a group by group name
     *
     * @param Request $request
     * @param $name
     * @return \Illuminate\Http\JsonResponse || void
     */
    public function showFeaturedByGroupName(Request $request, $name) {
        $group = Group::where('name', '=', $name)->first();
        $featured = $group->featuredActive;
        if($group) {
            $userAddress = GeoLocation::fromDegrees($request->latitude, $request->longitude);

            $featured->each(function($value, $key) use ($userAddress) {
                $response = GeoLocation::getGeocodeFromGoogle($value->address);
                $latitude = $response->results[0]->geometry->location->lat?:24.55;
                $longitude = $response->results[0]->geometry->location->lng?:24.55;
                $placeAddress = GeoLocation::fromDegrees($latitude, $longitude);
                $km = $userAddress->distanceTo($placeAddress, 'km');
                $value->km = $km;
                $value->meters = floatval(number_format($value->km*1000, 2, '.', ''));
                $value->load('thumbnail');
            });

            $featured = $this->calculateShortest($featured);

            return response()->json([
                'message' => 'Successfully retrieved featured for '.$name,
                'code' => 200,
                'entity' => $featured->values()
            ]);
        } else {
            return abort(404);
        }
    }

    /**
    * Showing 5 featured
    *
    * Sorts and returns only 5 of the elements
    * from list.
    *
    * @param $list
    * @return Illuminate\Support\Collection
    */
    private function calculateShortest($list) {
        $list = $list->sortBy(function($val, $key) {
            return $val->km;
        })->reject(function($val, $key) {
            return $val->km > 150;
        });

        return $list->count() > 0 ? $list->count() >= 5 ? $list->take(5) : $list->take($list->count()) : collect([]);
    }

    private function getCountry($lat, $long) {
        $json = $this->getGeocodeFromGoogle($lat, $long);
        return $json->results;
    }
    
    private function getGeocodeFromGeonames($latitude, $longitude) {
        $url = 'http://api.geonames.org/countryCodeJSON?lat='.$latitude.'&lng='.$longitude.'&username=nikola94';
        $ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL, $url);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    return json_decode(curl_exec($ch));
    }

    private function getGeocodeFromGoogle($latitude, $longitude) {
        //55.704093,13.193582
		$url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng='.$latitude.','.$longitude.'&sensor=false&key=AIzaSyAKfX6LiXd56wqoYTiI_qtsub_tCU93NMs';
		$ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL, $url);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    return json_decode(curl_exec($ch));
	}

    /**
     * Featured by name
     *
     * Retrieve single featured by it's name
     *
     * @param $name
     * @return \Illuminate\Http\JsonResponse
     */
    public function showFeaturedByName($name) {
        $featured = Featured::where('name', '=', $name)->first();
        if($featured) {
            $featured->load('images');

            return response()->json([
                'message' => 'Successfully retrieved featured for '.$name,
                'code' => 200,
                'entity' => $featured
            ]);
        } else {
            return abort(404);
        }
    }
}
