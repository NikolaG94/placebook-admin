<?php

namespace App\Http\Controllers;

use App\Ad;
use App\Settings;
use Illuminate\Http\Request;

class SettingsController extends Controller
{

    /**
     * Display the specified resource.
     *
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $settings = Settings::with('firstAd')->find(1);
        $ads = Ad::all()->pluck('name', 'id')->all();
        return view('admin.settings.single-view', ['settings' => $settings, 'ads' => $ads]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $settings = Settings::with('firstAd')->find(1);
        $settings->fill($request->all());
        $ad = Ad::find($request->firstAd);

        if($ad) {
            $settings->firstAd()->associate($ad);
        }
        $settings->save();
        $settings->load('firstAd');
        $ads = Ad::all()->pluck('name', 'id')->all();
        return view('admin.settings.single-view', ['settings' => $settings, 'ads' => $ads]);
    }
}
