<?php

namespace App\Http\Controllers;

use App\Ad;
use App\Facades\FileHandler;
use App\Helpers\Constant;
use App\Http\Requests\CreateAdRequest;
use App\Http\Requests\EditAdRequest;
use App\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class AdController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\View
     */
    public function index(Request $request)
    {
        $active = $request->has('active');
        $search = $request->has('search');

        if($active || $search) {
            $searchTerm = $request->get('search');
            $activeTerm = $request->get('active');
            $isNotEmpty = ($activeTerm==0 || $activeTerm==1) && $activeTerm!='';

            if ($isNotEmpty && !empty($searchTerm)) {
                $ads = Ad::where('active', $activeTerm==1)->where('name', 'LIKE', '%'.$searchTerm.'%')->get();
            } else if($isNotEmpty && empty($searchTerm)) {
                $ads = Ad::where('active', $activeTerm==1)->get();
            } else if(!$isNotEmpty && !empty($searchTerm)) {
                $ads = Ad::where('name', 'LIKE', '%'.$searchTerm.'%')->get();
            }
        }

        if(!$active && !$search) {
            $ads = Ad::all();
        }
        return view('admin.ad.index', ['ads' => $ads]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $ad = new Ad;
        return view('admin.ad.create', ['ad' => $ad]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateAdRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateAdRequest $request)
    {
        $ad = new Ad;
        $ad->fill($request->all());
        $ad->active = $request->active;
        $ad->save();
        $image = $request->image;
        $imagePath = Constant::IMAGES_PATH . FileHandler::uploadPath(Constant::absolutePath("IMAGES_PATH"))->addFile($image)->last();
        $image = new Image(['url' => $imagePath]);
        $ad->image()->save($image);
        return redirect()->route('ad.show', ['ad' => $ad]);
    }

    /**
     * Display the specified resource.
     *
     * @param  Ad $ad
     * @return \Illuminate\Http\Response
     */
    public function show(Ad $ad)
    {
        $ad->load('image');
        return view('admin.ad.single-view', ['ad' => $ad]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Ad $ad
     * @return \Illuminate\Http\Response
     */
    public function edit(Ad $ad)
    {
        return view('admin.ad.edit', ['ad' => $ad]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  EditAdRequest  $request
     * @param  Ad $ad
     * @return \Illuminate\Http\Response
     */
    public function update(EditAdRequest $request, Ad $ad)
    {
        $ad->fill($request->all());
        $ad->active = $request->active;
        $ad->save();
        if($request->hasFile('image')) {
            $image = $request->image;
            $imagePath = Constant::IMAGES_PATH . FileHandler::uploadPath(Constant::absolutePath("IMAGES_PATH"))->removeFile($ad->image->url)->addFile($image)->last();
            $ad->image->url = $imagePath;
            $ad->image->save();
        }

        return redirect()->route('ad.show', ['ad' => $ad]);
    }
}
