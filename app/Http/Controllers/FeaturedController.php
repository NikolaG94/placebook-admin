<?php

namespace App\Http\Controllers;

use App\Facades\FileHandler;
use App\Featured;
use App\Group;
use App\Helpers\Constant;
use App\Http\Requests\CreateFeaturedRequest;
use App\Http\Requests\EditFeaturedRequest;
use App\Image as ImageModel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;

class FeaturedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $active = $request->has('active');
        $search = $request->has('search');

        if($active || $search) {
            $groups = Group::whereHas('featured')->with(['featured' => function($query) use ($request, $search, $active) {
                $searchTerm = $request->get('search');
                $activeTerm = $request->get('active');
                $isNotEmpty = ($activeTerm==0 || $activeTerm==1) && $activeTerm!='';

                if($search && !empty($searchTerm))
                    $query->where('name', 'LIKE', '%'.$searchTerm.'%')->get();
                if($active && $isNotEmpty)
                    $query->where('active', $activeTerm == 1);
            }])->get();

        } else {
            $groups = Group::whereHas('featured')->with(['featured' => function($query) {
                $query->where('active', true);
            }])->get();
        }
        return view('admin.featured.index', ['groups' => $groups]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Group $group
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Group $group)
    {
        $featured = new Featured;
        $featured->group()->associate($group);
        return view('admin.featured.create', ['featured' => $featured]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\CreateFeaturedRequest $request
     * @param Group $group
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateFeaturedRequest $request, Group $group)
    {
        $featured = new Featured;
        $featured->fill($request->all());
        $featured->active = $request->active;
        $featured->group()->associate($group);
        $featured->save();
        $images = collect($request->images);
        $thumbnail = $request->thumbnail;
        $images = $images->map(function($image, $key) use ($featured) {
            $filePath = Constant::IMAGES_PATH . md5(Carbon::now() . $image->getClientOriginalName()) . '.' . $image->getClientOriginalExtension();
            Image::make($image)->resize(860, 640)->save(public_path($filePath));
            $image = new ImageModel(['url' => $filePath]);
            return $image;
        });
        $thumbnailPath = Constant::IMAGES_PATH . FileHandler::uploadPath(Constant::absolutePath("IMAGES_PATH"))->addFile($thumbnail)->last();
        $thumbnail = new ImageModel(['url' => $thumbnailPath, 'is_thumbnail' => true]);
        $featured->thumbnail()->save($thumbnail);
        $featured->images()->saveMany($images);
        return redirect()->route('featured.show', ['featured' => $featured]);
    }

    /**
     * Display the specified resource.
     *
     * @param Featured $featured
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Featured $featured)
    {
        $featured->load(['thumbnail', 'images' => function($query) {
            $query->where('is_thumbnail', false);
        }, 'group']);
        return view('admin.featured.single-view', ['featured' => $featured]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Featured $featured
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Featured $featured)
    {
        $featured->load(['thumbnail', 'images' => function($query) {
            $query->where('is_thumbnail', false);
        }]);
        return view('admin.featured.edit', ['featured' => $featured]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\EditFeaturedRequest $request
     * @param Featured $featured
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(EditFeaturedRequest $request, Featured $featured)
    {
        $featured = $featured->fill($request->all());
        $featured->active = $request->active;
        $featured->save();
        if($request->hasFile('thumbnail')) {
            $thumbnail = $request->thumbnail;
            $thumbnailPath = Constant::IMAGES_PATH . FileHandler::uploadPath(Constant::absolutePath("IMAGES_PATH"))->removeFile($featured->thumbnail->url)->addFile($thumbnail)->last();
            $featured->thumbnail->url = $thumbnailPath;
            $featured->thumbnail->save();
        }
        if($request->hasFile('images')) {
            $images = collect($request->images);
            $images = $images->map(function($image, $key) use ($featured) {
                $filePath = Constant::IMAGES_PATH . md5(Carbon::now() . $image->getClientOriginalName()) . '.' . $image->getClientOriginalExtension();
                Image::make($image)->resize(860, 640)->save(public_path($filePath));
                $image = new ImageModel(['url' => $filePath]);
                return $image;
            });
            $featured->images()->saveMany($images);
        }

        return redirect()->route('featured.show', ['featured' => $featured]);
    }
}
