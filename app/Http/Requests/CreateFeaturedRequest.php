<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateFeaturedRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Overrided
     *
     * @return array
     */
    public function messages() {
        return [
            'name.required' => '\'Ime objekta\' je obavezno polje.',
            'name.max' => 'Maksimalno 255 karaktera može da ima \'Ime objekta\'',
            'name.unique' => '\'Ime objekta\' polje nije jedinstveno, :value već postoji.',
            'address.required' => '\'Adresa objekta\' je obavezno polje.',
            'address.max' => 'Maksimalno 255 karaktera može da ima \'Adresa objekta\'',
            'work_hours.required' => '\'Radno Vreme\' je obavezno polje.',
            'work_hours.max' => 'Maksimalno 255 karaktera može da ima \'Radno Vreme\'',
            'images.required' => '\'Galerija\' je obavezno polje.',
            'images.between' => '\'Galerija\' polje mora da ima minimum 4, a maksimum 10 slika.',
            'images.*.image' => '\'Galerija\' polje sadrži fajl koji nije slika.',
            'images.*.mimes' => '\'Galerija\' polje sadrži fajl koji nije validan format slike.',
            'images.*.dimensions' => '\'Galerija\' polje sadrži fajl koji nije veličine 860x640.',
            'thumbnail.required' => '\'Thumbnail\' polje je obavezno.',
            'thumbnail.image' => '\'Thumbnail\' polje mora da bude slika.',
            'thumbnail.mimes' => '\'Thumbnail\' polje nema validan format slike.',
            'thumbnail.dimensions' => '\'Thumbnail\' slika nije veličine 860x640.'
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255|unique:featured',
            'address' => 'required|max:255',
            'work_hours' => 'required|max:13',
            'images' => 'required|array|between:4,10',
            'images.*' => 'sometimes|image|mimes:jpeg,gif,png,jpg,PNG,JPEG,JPG',
            'thumbnail' => 'required|image|mimes:jpeg,gif,png,jpg,PNG,JPEG,JPG|dimensions:width=860,height=640'
        ];
    }
}
