<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class EditFeaturedRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Overrided
     *
     * @return array
     */
    public function messages() {
        return [
            'name.unique' => '\'Ime objekta\' polje nije jedinstveno, :value već postoji.',
            'images.required' => '\'Galerija\' je obavezno polje.',
            'images.between' => '\'Galerija\' polje mora prelazi dozvoljenih 10 slika. (:max)',
            'images.*.image' => '\'Galerija\' polje sadrži fajl koji nije slika.',
            'images.*.mimes' => '\'Galerija\' polje sadrži fajl koji nije validan format slike.',
            'images.*.dimensions' => '\'Galerija\' polje sadrži fajl koji nije veličine 860x640.',
            'thumbnail.required' => '\'Thumbnail\' polje je obavezno.',
            'thumbnail.image' => '\'Thumbnail\' polje mora da bude slika.',
            'thumbnail.mimes' => '\'Thumbnail\' polje nema validan format slike.',
            'thumbnail.dimensions' => '\'Thumbnail\' slika nije veličine 860x640.'
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $currentCount = $this->route('featured')->images()->count();
        $total = 10;
        $maxPictures = $currentCount > $total;
        if(!$maxPictures) {
            $total -= $currentCount;
        }
        return [
            'name' => [Rule::unique('featured')->ignore($this->route('featured')->id)],
            'images' => 'sometimes|required|array|between:0,'.$total,
            'images.*' => 'sometimes|image|mimes:jpeg,gif,png,jpg,PNG,JPEG,JPG|dimensions:width=860,height=640',
            'thumbnail' => 'sometimes|required|image|mimes:jpeg,gif,png,jpg,PNG,JPEG,JPG|dimensions:width=860,height=640'
        ];
    }
}
