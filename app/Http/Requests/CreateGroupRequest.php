<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateGroupRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Overrided
     *
     * @return array
     */
    public function messages() {
        return [
            'name.required' => '\'Ime grupe\' je obavezno polje.',
            'name.max' => 'Maksimalno 60 karaktera može da ima \'Ime grupe\'',
            'name.unique' => '\'Ime grupe\' polje nije jedinstveno, :value već postoji.',
            'image' => '\'Logo slika\' polje sadrži fajl koji nije slika.',
            'image.required' => '\'Logo slika\' je obavezno polje.',
            'image.mimes' => '\'Logo slika\' polje sadrži fajl koji nije validan format slike.',
            'image.dimensions' => '\'Logo slika\' polje sadrži fajl koji nije veličine 71x71.'
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:60|unique:groups',
            'image' => 'required|image|mimes:jpeg,gif,png,jpg,PNG,JPEG,JPG|dimensions:width=71,height=71'
        ];
    }
}
