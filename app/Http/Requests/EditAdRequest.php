<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class EditAdRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Overrided
     *
     * @return array
     */
    public function messages() {
        return [
            'name.required' => '\'Ime reklame\' je obavezno polje.',
            'name.max' => 'Maksimalno 255 karaktera može da ima \'Ime reklame\'',
            'name.unique' => '\'Ime reklame\' polje nije jedinstveno, :value već postoji.',
            'link.url' => '\'Link\' polje nije validan url.',
            'image.required' => '\'Slika\' polje je obavezno.',
            'image.image' => '\'Slika\' polje mora da bude slika.',
            'image.mimes' => '\'Slika\' polje nema validan format slike.',
            'image.dimensions' => '\'Slika\' slika nije veličine 320x100.',
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'max:255', Rule::unique('ads')->ignore($this->route('ad')->id)],
            'link' => 'sometimes|required|url',
            'image' => 'sometimes|required|image|mimes:jpeg,gif,png,jpg,PNG,JPEG,JPG|dimensions:width=320,height=100'
        ];
    }
}
